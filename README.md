# YOLOv7 Noetic


## YOLOのバージョン
[バージョンの比較](https://qiita.com/tfukumori/items/519d84bf3feb8d246924)


## YOLOv7をDarknetで動かす
[Darknet対応](https://qiita.com/RyoWakabayashi/items/ff4f0bb5805a0125a9e2)

機械学習モデルを OpenCV で動かすためには、 dnn.readNet で読み込める形式が必要。

- *.caffemodel (Caffe, http://caffe.berkeleyvision.org/)
- *.pb (TensorFlow, https://www.tensorflow.org/)
- *.t7 | *.net (Torch, http://torch.ch/)
- *.weights (Darknet, https://pjreddie.com/darknet/)
- *.bin (DLDT, https://software.intel.com/openvino-toolkit)
- *.onnx (ONNX, https://onnx.ai/)

YOLOv3やYOLOv4ではDarknetの .weightを使用している。

下記のページからYOLOv7用の.cfg と .weightをダウンロードする。

https://github.com/AlexeyAB/darknet/tree/master/cfg

https://github.com/WongKinYiu/yolov7/releases/tag/v0.1



##



[darknet_ros](https://github.com/Ar-Ray-code/darknet_ros)
netic ブランチ
```
roslaunch darknet_ros yolov4-tiny.launch
```
```
roslaunch darknet_ros yolov4.launch
```

```
roslaunch darknet_ros yolov7-tiny.launch
```

GPUのメモリー不足で動かない
```
roslaunch darknet_ros yolov7.launch
```



## ROS1
[ROS package for official YOLOv7](https://github.com/lukazso/yolov7-ros)

## ROS2
[YOLOv7-tinyをdarknet-rosで使用する](https://ar-ray.hatenablog.com/entry/2022/07/25/080000)


[ROS NoeticでDarknet-rosは動くらしい（OpenCV4+CUDA11.2）](https://ar-ray.hatenablog.com/entry/2021/05/07/180000)

[darknet_rosをDockerfileで環境構築する。（Docker・ROS2）](https://ar-ray.hatenablog.com/entry/2021/12/08/112029)
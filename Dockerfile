# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
FROM nvidia/cudagl:11.3.0-devel-ubuntu20.04

LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="December ３1, 2023"

SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics
ENV __NV_PRIME_RENDER_OFFLOAD=1
ENV __GLX_VENDOR_LIBRARY_NAME=nvidia
ARG ROS_DISTRO=noetic


# Timezone, Launguage設定
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     language-pack-ja-base language-pack-ja \
     software-properties-common tzdata \
     fonts-ipafont fonts-ipaexfont fonts-takao
RUN  locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe
# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo


RUN apt-get update && apt-get install -y \
  build-essential cmake g++ \
  iproute2 gnupg gnupg1 gnupg2 \
  libcanberra-gtk* \
  python3-pip  python3-tk \
  git wget curl \
  x11-utils x11-apps terminator xterm xauth mesa-utils\
  nano vim htop \
  software-properties-common gdb valgrind sudo

# Install ROS Noetic
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
RUN apt-get update \
 && apt-get install -y --no-install-recommends ros-noetic-desktop-full
RUN apt-get install -y --no-install-recommends python3-rosdep
RUN rosdep init \
 && rosdep fix-permissions \
 && rosdep update


RUN apt-get update && apt-get install -y \
 ros-noetic-video-stream-opencv python3-catkin-tools ros-noetic-usb-cam


# Add user and group
# ARG {VARIABLE} are defined in .env
ARG UID
ARG GID
ARG USER_NAME
ARG GROUP_NAME
ARG PASSWORD
ARG WORKSPACE_DIR
RUN groupadd -g $GID $GROUP_NAME && \
    useradd -m -s /bin/bash -u $UID -g $GID -G sudo,video  $USER_NAME && \
    echo $USER_NAME:$PASSWORD | chpasswd && \
    echo "$USER_NAME   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USER_NAME}
RUN sudo usermod -a -G video ${USER_NAME}


RUN source /opt/ros/noetic/setup.bash && \
    mkdir -p ~/catkin_ws/src && cd ~/catkin_ws/src && \
    git clone https://github.com/Ar-Ray-code/darknet_ros -b noetic --recursive && \ 
    bash darknet_ros/rm_darknet_CMakeLists.sh && \
    cd .. && catkin build

# YOLOv7
COPY darknet/yolov7.launch /home/${USER_NAME}/catkin_ws/src/darknet_ros/darknet_ros/launch
COPY darknet/yolov7-tiny.launch /home/${USER_NAME}/catkin_ws/src/darknet_ros/darknet_ros/launch
COPY darknet/yolov7.yaml /home/${USER_NAME}/catkin_ws/src/darknet_ros/darknet_ros/config
COPY darknet/yolov7-tiny.yaml /home/${USER_NAME}/catkin_ws/src/darknet_ros/darknet_ros/config
RUN cd ~/catkin_ws/src/darknet_ros/darknet_ros/yolo_network_config/weights && \
    wget https://github.com/WongKinYiu/yolov7/releases/download/v0.1/yolov7.weights && \
    wget https://github.com/WongKinYiu/yolov7/releases/download/v0.1/yolov7-tiny.weights
RUN cd ~/catkin_ws/src/darknet_ros/darknet_ros/yolo_network_config/cfg && \
    wget https://raw.githubusercontent.com/AlexeyAB/darknet/master/cfg/yolov7.cfg && \
    wget https://raw.githubusercontent.com/AlexeyAB/darknet/master/cfg/yolov7-tiny.cfg

# Terminator Config
RUN mkdir -p /home/${USER_NAME}/.config/terminator/
COPY assets/terminator_config /home/${USER_NAME}/.config/terminator/config 
RUN sudo chmod 777 /home/${USER_NAME}/.config/terminator/config
COPY assets/entrypoint.sh /tmp/entrypoint.sh
RUN sudo chmod a+x /tmp/entrypoint.sh


# .bashrc
RUN echo "source /opt/ros/noetic/setup.bash" >> /home/${USER_NAME}/.bashrc
RUN echo "source /home/roboworks/catkin_ws/devel/setup.bash" >> /home/${USER_NAME}/.bashrc

#WORKDIR ${WORKSPACE_DIR}
ENTRYPOINT ["/tmp/entrypoint.sh"]
#CMD ["/bin/bash"]
CMD ["terminator"]



